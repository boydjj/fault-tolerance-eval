Fusion vs. Replication
----------------------
A socket-based investigation of active replication as a fault tolerance mechanism.

To run tests:

    $ pwd
    /Users/jboyd/Repos/fault-tolerance-eval
    $ py.test
    ...
    ===================================== 18 passed in 1.62 seconds =====================================

GroupManager
============
The GroupManager passes requests through to the data nodes it manages and multicasts data
changes to the group.

To start the GroupManager server:

    $ pwd
    /Users/jboyd/Repos/fault-tolerance-eval
    $ python -m fault_tolerance_eval.group_manager.app localhost 9999 4 examples/node_config.ini

To start the DataNode app:

    $ pwd
    /Users/jboyd/Repos/fault-tolerance-eval
    
    # In separate tabs:
    $ python -m fault_tolerance_eval.data_node.app examples/node_config.ini 1
    $ python -m fault_tolerance_eval.data_node.app examples/node_config.ini 2
    $ python -m fault_tolerance_eval.data_node.app examples/node_config.ini 3
    $ python -m fault_tolerance_eval.data_node.app examples/node_config.ini 4

To quickly test updates and check correctness:

    $ ipython examples/test_correctness.py 10 # will send 10 random bits to the 'A' group

Test runs and analysis
======================
See `experiment_results/README.md` for detail on how test runs were executed. All analysis
is based on the raw data present in the subdirectories there.
