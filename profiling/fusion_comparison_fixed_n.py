import argparse
import datetime
import logging
import os
import shlex
import signal
import subprocess
import sys
import time


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

client_cmd_tmpl = 'python -m fault_tolerance_eval.data_node.app examples/node_config.ini {}'

def launch_nodes(num_nodes):
    logger.debug('Launching %s nodes...', num_nodes)

    node_processes = {}
    for i in xrange(1, num_nodes):
        logger.info('Starting node %s', i)
        cmd_args = shlex.split(client_cmd_tmpl.format(i))
        node_process = subprocess.Popen(cmd_args)

        # Record node section ID for this process
        node_processes[node_process] = i
        time.sleep(2)
    return node_processes


def kill_node_processes(processes, recover=True):
    new_processes = {}
    for p, node_section_id in processes.iteritems():
        os.kill(p.pid, signal.SIGINT)
        if recover:
            time.sleep(3)
            client_cmd = shlex.split(client_cmd_tmpl.format(node_section_id))
            new_process = subprocess.Popen(client_cmd)
            new_processes[new_process] = node_section_id
    return new_processes


if __name__ == '__main__':
    comparison_start = datetime.datetime.now()
    logger.info('Beginning comparison at %s', comparison_start)
    comparison_complete = False

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Do a run of this active replication example system for comparison '
                    'with a similar fusion-based system\'s run.')
    parser.add_argument('-n', '--nodes', type=int, default=5,
                        help='Number of primaries to create.')
    parser.add_argument('-o', '--ops', type=int, default=100,
                        help='Number of operations per primary. All operations are '
                             'appends.')
    parser.add_argument('-s', '--step', type=int, default=10,
                        help='Interval at which to stop and recover all primaries.')
    args = parser.parse_args()

    logger.info('Starting group manager...')
    cmd = 'python -m fault_tolerance_eval.group_manager.app localhost 9999 {} ' \
          'examples/node_config.ini'.format(args.nodes)
    cmd_args = shlex.split(cmd)
    group_manager_process = subprocess.Popen(cmd_args)
    logger.debug('Server process ID: %s', group_manager_process.pid)

    # Give group manager time to set up
    time.sleep(5)

    node_processes = launch_nodes(args.nodes + 1)

    # Give all nodes time to set up
    time.sleep(5)

    ops_performed = 0
    ops_cmd_tmpl = 'python examples/test_correctness.py {}'

    while ops_performed < args.ops:
        # Synchronously call test script and wait on its completion
        subprocess.call(shlex.split(ops_cmd_tmpl.format(args.step)))
        ops_performed += args.step
        node_processes = kill_node_processes(node_processes)

    if ops_performed >= args.ops:
        comparison_complete = True

    # Stay alive so ctrl-C gets passed to child processes
    try:
        while True:
            if comparison_complete:
                comparison_end = datetime.datetime.now()
                comparison_duration = comparison_end - comparison_start
                logger.info('Ending comparison at %s. Seconds elapsed: %s', comparison_end,
                    comparison_duration.total_seconds())
                kill_node_processes(node_processes, False)
                os.kill(group_manager_process.pid, signal.SIGINT)
                sys.exit(0)
            time.sleep(5)
    except KeyboardInterrupt:
        logger.debug('Got interrupt, wrapping up.')
