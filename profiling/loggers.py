"""
Set up several loggers for use during profiling.
"""
import logging

csv_formatter = logging.Formatter('%(process)d,"%(asctime)s",%(name)s,%(levelname)s,%(message)s')


def get_backup_size_logger_for_node(group_id, node_id):
    # If multiple processes are running for the same node ID, this is an issue. But then
    # you have other issues anyway.
    filename = '{group_id}-{node_id}_backup_size_log.csv'.format(group_id=group_id,
                                                                 node_id=node_id)
    backup_size_handler = logging.FileHandler(filename)
    backup_size_handler.setFormatter(csv_formatter)

    backup_size_logger = logging.getLogger('backup_size')
    backup_size_logger.addHandler(backup_size_handler)
    return backup_size_logger

MULTICAST_MSG_COUNT_FILENAME = 'multicast_msg_count_log.csv'
multicast_msg_count_handler = logging.FileHandler(MULTICAST_MSG_COUNT_FILENAME)
multicast_msg_count_handler.setFormatter(csv_formatter)

multicast_msg_count_logger = logging.getLogger('multicast_msg_count')
multicast_msg_count_logger.addHandler(multicast_msg_count_handler)

UPDATE_TIME_FILENAME = 'update_time_log.csv'
update_time_handler = logging.FileHandler(UPDATE_TIME_FILENAME)
update_time_handler.setFormatter(csv_formatter)

update_time_logger = logging.getLogger('update_time')
update_time_logger.addHandler(update_time_handler)

RECOVERY_TIME_FILENAME = 'recovery_time.csv'
recovery_time_handler = logging.FileHandler(RECOVERY_TIME_FILENAME)
recovery_time_handler.setFormatter(csv_formatter)

recovery_time_logger = logging.getLogger('recovery_time')
recovery_time_logger.addHandler(recovery_time_handler)
