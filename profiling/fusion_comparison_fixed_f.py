import argparse
import datetime
import logging
import os
import shlex
import signal
import subprocess
import sys
import time

import collections

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

NODE_CONFIG_FILE = 'node_config_f.ini'
CLIENT_CMD_TMPL = 'python -m fault_tolerance_eval.data_node.app examples/{} {}'
CORRECTNESS_SCRIPT_NAME = 'test_correctness_fixed_f.py'


def launch_nodes(num_nodes):
    logger.debug('Launching %s nodes...', num_nodes)

    # bit of a hack for making sure we're crashing/recovering nodes group-by-group
    GROUPS = {
        1: 'A',
        2: 'A',
        3: 'B',
        4: 'B',
        5: 'C',
        6: 'C',
        7: 'D',
        8: 'D',
        9: 'E',
        10: 'E',
    }

    # this is a mapping from {<group id>: {<process>: <config section ID>, ... }}
    node_processes = collections.defaultdict(dict)

    # xrange(start, end) is exclusive of end
    for i in xrange(1, num_nodes + 1):
        logger.info('Starting node %s', i)
        cmd_args = shlex.split(CLIENT_CMD_TMPL.format(NODE_CONFIG_FILE, i))

        node_process = subprocess.Popen(cmd_args)

        # Record node section ID for this process
        node_processes[GROUPS[i]][node_process] = i
        time.sleep(2)

    logger.debug('Launched nodes: %s', node_processes)
    return node_processes


def kill_node_processes(processes, recover=True):
    new_processes = collections.defaultdict(dict)

    for group, node_mapping in processes.iteritems():
        for p, node_section_id in node_mapping.iteritems():
            logger.warn('Murdering process %s, node section %s', p.pid, node_section_id)
            os.kill(p.pid, signal.SIGINT)

            if recover:
                time.sleep(3)
                logger.warn('Restarting node %s', node_section_id)
                client_cmd = shlex.split(CLIENT_CMD_TMPL.format(NODE_CONFIG_FILE,
                                                                node_section_id))
                new_process = subprocess.Popen(client_cmd)
                time.sleep(3)
                new_processes[group][new_process] = node_section_id

    logger.debug('Launched new processes: %s', new_processes)

    return new_processes


if __name__ == '__main__':
    comparison_start = datetime.datetime.now()
    logger.info('Beginning comparison at %s', comparison_start)
    comparison_complete = False

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Do a run of this active replication example system for comparison '
                    'with a similar fusion-based system\'s run.')
    parser.add_argument('-f', '--fault_tolerance', type=int, default=1,
                        help='Number of crash faults to tolerate.')
    parser.add_argument('-g', '--groups', type=int, default=1,
                        help='Number of replication groups to support.')
    parser.add_argument('-o', '--ops', type=int, default=100,
                        help='Number of operations per primary. All operations are '
                             'appends.')
    parser.add_argument('-s', '--step', type=int, default=10,
                        help='Interval at which to stop and recover all primaries.')
    args = parser.parse_args()

    # For each group, we're going to launch f+1 nodes
    num_nodes = args.groups * (args.fault_tolerance + 1)

    logger.info('Starting group manager...')
    cmd = 'python -m fault_tolerance_eval.group_manager.app localhost 9999 {} ' \
          'examples/{}'.format(num_nodes, NODE_CONFIG_FILE)
    cmd_args = shlex.split(cmd)
    group_manager_process = subprocess.Popen(cmd_args)
    logger.debug('Server process ID: %s', group_manager_process.pid)

    # Give group manager time to set up
    time.sleep(5)

    node_processes = launch_nodes(num_nodes)

    # Give all nodes time to set up
    time.sleep(5)

    ops_performed = 0
    ops_cmd_tmpl = 'python examples/{} {} {}'

    while ops_performed < args.ops:
        # Synchronously call test script and wait on its completion
        logger.info('Calling %s', CORRECTNESS_SCRIPT_NAME)
        subprocess.call(shlex.split(ops_cmd_tmpl.format(CORRECTNESS_SCRIPT_NAME, args.step, args.groups)))
        ops_performed += args.step
        node_processes = kill_node_processes(node_processes)

    if ops_performed >= args.ops:
        comparison_complete = True

    # Stay alive so ctrl-C gets passed to child processes
    try:
        while True:
            if comparison_complete:
                comparison_end = datetime.datetime.now()
                comparison_duration = comparison_end - comparison_start
                logger.info('Ending comparison at %s. Seconds elapsed: %s', comparison_end,
                    comparison_duration.total_seconds())
                kill_node_processes(node_processes, False)
                os.kill(group_manager_process.pid, signal.SIGINT)
                sys.exit(0)
            time.sleep(5)
    except KeyboardInterrupt:
        logger.debug('Got interrupt, wrapping up.')
