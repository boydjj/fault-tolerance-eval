import logging

from profiling.loggers import get_backup_size_logger_for_node
from profiling.size import total_size

logger = logging.getLogger(__name__)

backup_size_logger = None


class ReplicatedDataNode(object):
    def __init__(self, group_id, proc_id, data=None):
        self.group_id = group_id
        # REALLY this is the Node ID
        self.node_id = proc_id

        if data is None:
            self.data = []
        else:
            self.data = data

        global backup_size_logger
        if backup_size_logger is None:
            backup_size_logger = get_backup_size_logger_for_node(self.group_id, self.node_id)

    def __str__(self):
        return 'DataNode {}:{} with {} elements'.format(self.group_id, self.node_id,
                                                        len(self.data))

    def add_int(self, value):
        self.data.append(value)
        backup_size_logger.info('%s,%s,%s,%s', self.group_id, self.node_id, len(self.data), total_size(self.data))
