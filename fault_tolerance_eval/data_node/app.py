import argparse
from ConfigParser import SafeConfigParser
import contextlib
import json
import logging
import socket
import SocketServer
import threading
import time

from .replicated import ReplicatedDataNode

from ..networking import multicast
from ..networking import util
from ..networking.protocol import Protocol

logger = logging.getLogger(__name__)


class GroupManagerException(Exception):
    pass


class MulticastMessageHandler(object):
    def __init__(self, node):
        self._node = node

    def handle(self, msg):
        logger.debug('Got msg: %s', msg)
        deserialized = json.loads(msg.content)
        logger.debug(deserialized)
        logger.debug('Node data before: %s', self._node.data)

        if deserialized['method'] == Protocol.ADD_INT:
            self._node.add_int(deserialized['value'])
        logger.debug('Node data after: %s', self._node.data)


class DataNodeReadServer(util.ReusableThreadingTCPServer):
    def __init__(self, server_address, handler_class, data_node, bind_and_activate=True):
        util.ReusableThreadingTCPServer.__init__(self, server_address, handler_class,
                                                 bind_and_activate)

        self.data_node = data_node
        logger.debug('Initialized read server with data node %s', self.data_node)


class DataNodeReadRequestHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        payload = self.rfile.readline().strip()
        logger.debug('Got payload from client: %s', payload)
        request = json.loads(payload)

        response = {}
        if request['method'] == Protocol.READ_ALL:
            node_data = self.server.data_node.data
            response = Protocol.get_read_all_response(self.server.data_node.group_id,
                                                      node_data)

        self.write_response(Protocol.serialize(response))

    def write_response(self, content):
        self.wfile.write('{response}'.format(response=content))


class ReplicatedDataNodeApp(object):
    NUM_MULTICAST_WORKERS = 2

    def __init__(self, config_file, section_id):
        logger.debug('Initializing with config_file %s, section_id %s', config_file,
                     section_id)
        parser = SafeConfigParser()
        parser.read(config_file)
        logger.debug('Config parser: %s', parser)

        section = 'node {}'.format(section_id)
        self._group_id = parser.get(section, 'group_id')

        mgr_host = parser.get(section, 'mgr_host')
        mgr_port = parser.getint(section, 'mgr_port')
        self._mgr_addr = (mgr_host, mgr_port)

        multicast_host = parser.get(section, 'multicast_host')
        multicast_port = parser.getint(section, 'multicast_port')
        self._multicast_addr = (multicast_host, multicast_port)

        read_host = parser.get(section, 'read_host')
        read_port = parser.getint(section, 'read_port')
        self._read_addr = (read_host, read_port)

        self._proc_id = parser.getint(section, 'proc_id')
        self._group_id = parser.get(section, 'group_id')

    def __get_mgr_client(self):
        logger.debug('Creating socket to communicate with %s', self._mgr_addr)
        return socket.create_connection(self._mgr_addr)

    def run(self):
        """
        The entry point for the DataNode application to run.
        """
        # Check with manager to see if we need to catch up on any existing data.
        initial_data = self._sync_with_mgr() or []

        self._node = ReplicatedDataNode(self._group_id, self._proc_id, initial_data)

        # Start listening for multicast messages.
        handler = MulticastMessageHandler(self._node)
        multicast_server = multicast.multicast_serve(self._multicast_addr,
                                                     self.NUM_MULTICAST_WORKERS,
                                                     handler.handle)

        logger.debug('Initializing read server at %s', self._read_addr)
        read_server = DataNodeReadServer(self._read_addr, DataNodeReadRequestHandler, self._node)
        server_thread = threading.Thread(target=read_server.serve_forever)
        server_thread.start()

        # Let the group manager know we're open for business.
        self._finalize_entry()

        return multicast_server, read_server

    def _sync_with_mgr(self):
        with contextlib.closing(self.__get_mgr_client()) as mgr_client:
            logger.debug('Initializing sync with manager at %s.', self._mgr_addr)
            msg = Protocol.get_entry(self._group_id, self._proc_id)
            payload = Protocol.serialize(msg)
            mgr_client.sendall(payload)
            mgr_response = json.loads(util.recv_until(mgr_client))
            logger.debug('Manager response (type %s): %s', type(mgr_response), mgr_response)

            if mgr_response['method'] == Protocol.ENTRY_SYNC:
                logger.debug('Got initial data back from manager.')
                response = mgr_response['data']
            else:
                logger.debug('No initial data received from manager.')
                response = None
            return response

    def _finalize_entry(self):
        with contextlib.closing(self.__get_mgr_client()) as mgr_client:
            logger.debug('Finalizing entry with manager.')
            msg = Protocol.get_entry_finalization(self._group_id, self._proc_id)
            payload = Protocol.serialize(msg)
            mgr_client.sendall(payload)
            response = json.loads(util.recv_until(mgr_client))

            if response['method'] != Protocol.ENTRY_FINALIZE_ACK:
                raise GroupManagerException(
                    'The manager did not accept our finalization.')

            logger.info('The manager ACKed our finalization. We can go live.')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s:%(levelname)-5s(%(threadName)-10s:%(name)s): %(message)s')
    arg_parser = argparse.ArgumentParser(description='ReplicatedDataNodeApp')
    arg_parser.add_argument('node_config', action='store',
                            help='The location of a config file for this client to use.')
    arg_parser.add_argument('config_section', action='store',
                            help='The section # within the node config for this client.')

    args = arg_parser.parse_args()

    app = ReplicatedDataNodeApp(args.node_config, args.config_section)
    logger.info('Starting server...')
    multicast_server, read_server = app.run()

    try:
        logger.info('Beginning wait loop. Hit Ctrl-C to kill server.')
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        logger.debug('Got KeyboardInterrupt, shutting down multicast server...')
        multicast_server.shutdown()
        logger.debug('Shutting down read server...')
        read_server.shutdown()
        logger.debug('Closing multicast server...')
        multicast_server.server_close()
        logger.debug('Closing read server...')
        read_server.server_close()
    finally:
        main_thread = threading.current_thread()
        child_threads_done = True
        for t in threading.enumerate():
            if t is main_thread:
                continue
            thread_name = t.getName()
            logger.info('Giving %s 1s to shut down...', thread_name)
            t.join(1)
            if t.isAlive():
                logger.info('%s did not shut down in time, killing.', thread_name)

    logger.debug('Shutdown complete.')
