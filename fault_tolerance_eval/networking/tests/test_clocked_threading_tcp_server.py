import SocketServer
import socket
import threading

import pytest

from .. import clock
from .. import multicast

HOST, PORT = "localhost", 9999


@pytest.fixture(scope='module')
def handler_cls():
    class EchoHandler(SocketServer.StreamRequestHandler):
        def handle(self):
            self.data = self.rfile.readline().strip()
            self.wfile.write(self.data.upper())
    return EchoHandler


@pytest.fixture(scope='function')
def server(request, handler_cls):
    """
    Set up a new server each time we run a test.
    """
    server = multicast.ClockedThreadingTCPServer((HOST, PORT), handler_cls)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    def fin():
        server.shutdown()
        server.server_close()

    request.addfinalizer(fin)
    return server


class TestClockedThreadingTCPServer(object):
    def test_clock_ticks_before_dispatch(self, server):
        current_timestamp = clock.clock.counter
        client = socket.create_connection((HOST, PORT))
        client.send('foo\n')
        result = client.recv(1024)

        assert result == 'FOO'
        assert clock.clock.counter == current_timestamp + 1
