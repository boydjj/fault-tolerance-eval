import json
import uuid

from .. import multicast
from ..clock import clock


class TestMulticastMessage(object):
    def test_constructor(self):
        new_uuid = uuid.uuid4()
        current_timestamp = clock.counter

        content = 'Hello, world!'
        msg = multicast.MulticastMessage(msg_id=new_uuid, content=content)

        assert msg.timestamp == current_timestamp
        assert msg.status == multicast.MULTICAST_STATUS_TENTATIVE
        assert msg.msg_id == str(new_uuid)
        assert msg.content == content

    def test_constructor_from_dict(self):
        new_uuid = uuid.uuid4()
        message = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid,
            'content': 'Hello, world!',
        }
        m = multicast.MulticastMessage(**message)
        assert m.timestamp == message['timestamp']
        assert m.status == message['status']
        assert m.msg_id == str(new_uuid)
        assert m.content == message['content']
        assert not m.deliverable

    def test_eq_same_args(self):
        new_uuid = uuid.uuid4()
        message = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid,
            'content': 'Hello, world!',
        }
        m1 = multicast.MulticastMessage(**message)
        m2 = multicast.MulticastMessage(**message)

        assert m1 == m2

    def test_eq_diff_args(self):
        new_uuid = uuid.uuid4()

        # Different contents but the same UUID
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid,
            'content': 'Goodbye, world!',
        }
        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        assert m1 == m2

    def test_eq_diff_msg_ids(self):
        new_uuid1 = uuid.uuid4()
        new_uuid2 = uuid.uuid4()

        # Different UUIDs but the same otherwise
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid1,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid2,
            'content': 'Hello, world!',
        }

        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        assert m1 != m2

    def test_cmp(self):
        new_uuid1 = uuid.uuid4()
        new_uuid2 = uuid.uuid4()

        # Different UUIDs and timestamps
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid1,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12346,
            'status': 'final',
            'msg_id': new_uuid2,
            'content': 'Hello, world!',
        }

        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        assert m1 != m2
        assert m1 < m2
        assert m2 > m1
