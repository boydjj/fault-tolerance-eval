from .. import clock


class TestClock(object):
    def test_init(self):
        assert clock.clock.counter == 0

    def test_singleton(self):
        new_clock = clock.Clock()
        assert clock.clock.instance is new_clock.instance
        assert new_clock.counter == clock.clock.counter

    def test_tick(self):
        current_timestamp = clock.clock.counter
        clock.clock.tick()
        assert clock.clock.counter == current_timestamp + 1

    def test_tick_other_instance(self):
        current_timestamp = clock.clock.counter
        new_clock = clock.Clock()

        assert new_clock.counter == current_timestamp

        clock.clock.tick()
        assert new_clock.counter == current_timestamp + 1
        assert clock.clock.counter == new_clock.counter

    def test_recv_msg_with_higher_timestamp(self):
        current_timestamp = clock.clock.counter

        new_timestamp = clock.clock.recv_msg(current_timestamp+20)

        assert new_timestamp == current_timestamp + 21

    def test_recv_msg_with_lower_timestamp(self):
        current_timestamp = clock.clock.counter

        new_timestamp = clock.clock.recv_msg(current_timestamp-1)

        assert new_timestamp == current_timestamp + 1
