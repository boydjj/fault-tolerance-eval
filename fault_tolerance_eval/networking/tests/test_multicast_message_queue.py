import uuid

from .. import multicast


class TestMulticastMessageQueue(object):
    def test_put_msg_in_queue(self):
        new_uuid1 = uuid.uuid4()
        new_uuid2 = uuid.uuid4()

        # Different UUIDs and timestamps
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid1,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12346,
            'status': 'final',
            'msg_id': new_uuid2,
            'content': 'Hello, world!',
        }

        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        multicast.MulticastMessageQueue.put(m1)
        multicast.MulticastMessageQueue.put(m2)

        q = multicast.MulticastMessageQueue.MESSAGE_DELIVERY_QUEUE
        assert q.not_empty
        assert m1 is q.get()
        assert m2 is q.get()
        assert q.empty()

    def test_msg_index(self):
        new_uuid1 = uuid.uuid4()
        new_uuid2 = uuid.uuid4()

        # Different UUIDs and timestamps
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid1,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12346,
            'status': 'final',
            'msg_id': new_uuid2,
            'content': 'Hello, world!',
        }

        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        multicast.MulticastMessageQueue.put(m1)
        multicast.MulticastMessageQueue.put(m2)

        assert multicast.MulticastMessageQueue.find(m1.msg_id) is m1
        assert multicast.MulticastMessageQueue.find(m2.msg_id) is m2
        assert multicast.MulticastMessageQueue.find('N/A') is None

    def test_finalize(self):
        new_uuid1 = uuid.uuid4()
        new_uuid2 = uuid.uuid4()

        # Different UUIDs and timestamps
        s1 = {
            'timestamp': 12345,
            'status': 'final',
            'msg_id': new_uuid1,
            'content': 'Hello, world!',
        }
        s2 = {
            'timestamp': 12346,
            'status': 'final',
            'msg_id': new_uuid2,
            'content': 'Hello, world!',
        }

        m1 = multicast.MulticastMessage(**s1)
        m2 = multicast.MulticastMessage(**s2)

        multicast.MulticastMessageQueue.put(m1)
        multicast.MulticastMessageQueue.put(m2)
        multicast.MulticastMessageQueue.finalize(m1.msg_id, 10)

        finalized_msg = multicast.MulticastMessageQueue.find(m1.msg_id)
        assert m1 is finalized_msg
        assert finalized_msg.timestamp == 10
        assert finalized_msg.deliverable == True
