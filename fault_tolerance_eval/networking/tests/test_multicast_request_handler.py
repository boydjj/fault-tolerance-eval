import json
import logging
import SocketServer
import socket
import threading

import pytest

from .. import clock
from .. import multicast
from .. import util

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

HOST, PORT = "localhost", 9999


@pytest.fixture(scope='module')
def handler_cls():
    return multicast.MulticastRequestHandler


@pytest.fixture(scope='function')
def server(request, handler_cls):
    """
    Set up a new server for this module.
    """
    class TestServer(SocketServer.ThreadingTCPServer):
        allow_reuse_address = True
    server = TestServer((HOST, PORT), handler_cls)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    def fin():
        server.shutdown()
        server.server_close()

    request.addfinalizer(fin)
    return server


class TestMulticastRequestHandler(object):
    def test_tentative_msg_put_on_queue(self, server):
        current_timestamp = clock.clock.counter
        client = socket.create_connection((HOST, PORT))
        msg = multicast.MulticastMessage('HELLO WORLD')
        client.sendall(msg.to_json() + '\n')

        result = util.recv_until(client)
        deserialized_result = json.loads(result)

        assert deserialized_result == {
            'msg_id': msg.msg_id,
            'proposed_timestamp': current_timestamp + 1,
            'multicast_timestamp': current_timestamp + 2,
        }

        queue_head = multicast.MulticastMessageQueue.find(str(msg.msg_id))
        assert queue_head is not None
        assert queue_head.timestamp == current_timestamp + 1
        assert queue_head.status == multicast.MULTICAST_STATUS_TENTATIVE
        assert queue_head.content == 'HELLO WORLD'
        assert not queue_head.deliverable

    def test_finalized_msg_marked_deliverable(self, server):
        # First we enqueue a message
        current_timestamp = clock.clock.counter
        client = socket.create_connection((HOST, PORT))
        msg = multicast.MulticastMessage('HELLO WORLD')
        logger.debug('First sending server: %s', msg)
        client.sendall(msg.to_json() + '\n')
        result = util.recv_until(client)
        logger.debug('First client response: %s', result)

        expected_assigned_timestamp = current_timestamp + 1

        deserialized_result = json.loads(result)

        assert deserialized_result == {
            'msg_id': msg.msg_id,
            'proposed_timestamp': expected_assigned_timestamp,
            'multicast_timestamp': current_timestamp + 2,
        }

        # Update to assigned timestamp
        msg.status = multicast.MULTICAST_STATUS_FINAL
        msg.timestamp = expected_assigned_timestamp
        logger.debug('Second sending server: %s', msg)

        # Set up a new client because SocketServer has already shut down our request :(
        client = socket.create_connection((HOST, PORT))

        client.sendall(msg.to_json() + '\n')
        result = util.recv_until(client)
        logger.debug('Second client response: %s', result)
        deserialized_result = json.loads(result)
        assert deserialized_result['ack']

        queue_head = multicast.MulticastMessageQueue.find(str(msg.msg_id))
        assert queue_head is not None
        assert queue_head.timestamp == expected_assigned_timestamp
        assert queue_head.status == multicast.MULTICAST_STATUS_FINAL
        assert queue_head.content == 'HELLO WORLD'
        assert queue_head.deliverable
