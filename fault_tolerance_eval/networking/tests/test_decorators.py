import logging

from ..protocol import node_message

logger = logging.getLogger(__name__)


class TestDecorators(object):
    def test_node_message(self):
        @node_message
        def myfunc(a, b):
            return {'a': a, 'b': b}

        assert myfunc(1, 2) == {'client_type': 'node', 'a': 1, 'b': 2}
