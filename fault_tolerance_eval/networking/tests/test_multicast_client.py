import logging
import SocketServer
import threading

import pytest

logger = logging.getLogger(__name__)

HOST, PORT = "localhost", 9999


@pytest.fixture(scope='function')
def server(request, handler_cls):
    """
    Set up a new server for this module.
    """
    class TestHandler(SocketServer.StreamRequestHandler):
        def handle(self):
            msg = self.rfile.read()
            ts, _ = msg.split(' ', 1)
            proposed_ts = ts + 1
            irrelevant_ts = 1337

            self.wfile.write('{} {}\n'.format(irrelevant_ts, proposed_ts))

    class TestServer(SocketServer.ThreadingTCPServer):
        allow_reuse_address = True

    server = TestServer((HOST, PORT), TestHandler)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    def fin():
        server.shutdown()
        server.server_close()

    request.addfinalizer(fin)
    return server
