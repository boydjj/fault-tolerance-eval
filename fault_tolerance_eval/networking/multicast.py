"""
Primitives for multicasting with the ABCAST protocol.
"""
import json
import logging
import Queue
import SocketServer
import socket
import threading
import uuid

from profiling.loggers import multicast_msg_count_logger
from .clock import clock
from . import util

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s (%(threadName)-10s) %(message)s')
logger = logging.getLogger(__name__)

MULTICAST_STATUS_TENTATIVE = 'tentative'
MULTICAST_STATUS_FINAL = 'final'


class NotDeliverable(Exception):
    pass


class MulticastMessage(object):
    MESSAGE_TEMPLATE = "{timestamp} {status} {msg_id} {content}"

    def __init__(self, content, timestamp=None, status=None, msg_id=None):
        self.timestamp = timestamp if timestamp else clock.counter
        self.status = status if status else MULTICAST_STATUS_TENTATIVE
        self.msg_id = str(msg_id) if msg_id else str(uuid.uuid4())
        self.content = content

        # All messages are initially undeliverable and may only be marked deliverable
        # after they are finalized in the second round of multicasting.
        self.deliverable = False

    def to_json(self):
        return json.dumps({
            'timestamp': self.timestamp,
            'status': self.status,
            'msg_id': self.msg_id,
            'content': self.content,
        })

    def __eq__(self, other):
        if not isinstance(other, MulticastMessage):
            return False
        return self.msg_id == other.msg_id

    def __ne__(self, other):
        if not isinstance(other, MulticastMessage):
            return True
        return self.msg_id != other.msg_id

    def __cmp__(self, other):
        return cmp(self.timestamp, other.timestamp)

    def __str__(self):
        return self.MESSAGE_TEMPLATE.format(
            timestamp=self.timestamp,
            status=self.status,
            msg_id=self.msg_id,
            content=self.content
        )

    def __repr__(self):
        return "{}(timestamp={}, status='{}', msg_id='{}', content='{}')".format(
            self.__class__.__name__,
            self.timestamp,
            self.status,
            self.msg_id,
            self.content
        )


class MulticastMessageQueue(object):
    """
    Manage indexing and queueing of messages.

    TODO: does `finalize` need to be made thread-safe?
    """
    get_deliverable_lock = threading.Lock()

    MESSAGE_INDEX = {}
    MESSAGE_DELIVERY_QUEUE = Queue.PriorityQueue()

    @classmethod
    def get(cls):
        with cls.get_deliverable_lock:
            msg = cls.MESSAGE_DELIVERY_QUEUE.get()
            if not msg.deliverable:
                cls.MESSAGE_DELIVERY_QUEUE.put(msg)
                raise NotDeliverable('Message {} at head of queue is not deliverable yet.'
                                     .format(msg.msg_id))
        return msg

    @classmethod
    def put(cls, msg):
        cls.MESSAGE_INDEX[msg.msg_id] = msg
        return cls.MESSAGE_DELIVERY_QUEUE.put(msg)

    @classmethod
    def find(cls, msg_id):
        return cls.MESSAGE_INDEX.get(msg_id)

    @classmethod
    def finalize(cls, msg_id, timestamp):
        msg = cls.MESSAGE_INDEX.get(msg_id)
        if msg is None:
            raise ValueError('No message could be found with msg_id %s', msg_id)
        msg.timestamp = timestamp
        msg.status = MULTICAST_STATUS_FINAL
        msg.deliverable = True


class ClockedThreadingTCPServer(SocketServer.ThreadingTCPServer):
    """
    A TCP server that spawns threads when handling requests and increments a Lamport clock
    for every received message.
    """
    allow_reuse_address = True

    def process_request(self, request, client_address):
        clock.tick()
        SocketServer.ThreadingTCPServer.process_request(self, request, client_address)
        return


class MulticastRequestHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        msg = self.rfile.readline().strip()
        payload = json.loads(msg)
        parsed_msg = MulticastMessage(**payload)
        logger.debug('Received MulticastMessage %s', parsed_msg)

        clock.recv_msg(parsed_msg.timestamp)

        if parsed_msg.status == MULTICAST_STATUS_TENTATIVE:
            # Update timestamp to the latest timestamp after recv_msg
            parsed_msg.timestamp = clock.counter
            MulticastMessageQueue.put(parsed_msg)
            logger.debug('Responding with proposed timestamp %s', parsed_msg.timestamp)
            response = {
                'msg_id': parsed_msg.msg_id,
                'proposed_timestamp': parsed_msg.timestamp
            }
            self.write_response(response)
        elif parsed_msg.status == MULTICAST_STATUS_FINAL:
            logger.debug('Finalizing message %s', parsed_msg.msg_id)
            # Write finalized timestamp to previously enqueued msg
            try:
                MulticastMessageQueue.finalize(parsed_msg.msg_id, parsed_msg.timestamp)
            except ValueError:
                # For some reason the finalized message isn't in our queue, but since it's
                # final we can go ahead and add it
                MulticastMessageQueue.put(parsed_msg)
            response = {
                'msg_id': parsed_msg.msg_id,
                'ack': True
            }
            self.write_response(response)

    def write_response(self, response):
        clock.tick()
        response['multicast_timestamp'] = clock.counter
        logger.debug('Writing serialized version of: %s', response)
        serialized_response = json.dumps(response)
        self.wfile.write(serialized_response + '\n')


class MulticastClient(object):
    def __init__(self, group_hosts, delimiter='\n'):
        """
        A client that knows how to propose and then finalize a message.

        :param group_hosts: a list of (HOST, PORT) tuples to multicast a message to
        """
        self.group_hosts = group_hosts
        self.delimiter = delimiter

    def remove_host(self, host):
        self.group_hosts.remove(host)

    def add_host(self, host):
        self.group_hosts.append(host)

    def send(self, msg):
        proposed_msg = MulticastMessage(msg)
        proposals = self._propose(proposed_msg)

        final_timestamp = max(proposals)
        proposed_msg.status = MULTICAST_STATUS_FINAL
        proposed_msg.timestamp = final_timestamp
        acks = self._finalize(proposed_msg)
        for ack in acks:
            if not ack:
                logger.warn('Got a non-OK response from a host!')

    def _send_to_group(self, msg):
        """
        Sends `msg` to all members of the group and returns their (raw) responses.
        """
        responses = []
        for host in self.group_hosts:
            logger.debug('Getting socket for host %s', host)
            try:
                client = socket.create_connection(host)
            except:
                logger.warn('Could not create connection with %s', host)
                continue
            client.sendall(msg.to_json() + self.delimiter)
            response = util.recv_until(client, self.delimiter)
            responses.append(json.loads(response))
        return responses

    def _propose(self, proposed_msg):
        raw_responses = self._send_to_group(proposed_msg)
        proposed_timestamps = []
        for response in raw_responses:
            assert response['msg_id'] == proposed_msg.msg_id
            proposed_timestamps.append(response['proposed_timestamp'])
        return proposed_timestamps

    def _finalize(self, msg):
        raw_responses = self._send_to_group(msg)
        acks = []
        for response in raw_responses:
            assert response['msg_id'] == msg.msg_id
            acks.append(response['ack'])
        return acks


class MulticastWorker(object):
    @classmethod
    def _watch_message(cls, handler):
        logger.debug("Waiting for a deliverable message...")
        counter = 0
        while True:
            try:
                msg = MulticastMessageQueue.get()
            except NotDeliverable as exc:
                logger.debug(exc.message)
            else:
                logger.debug('Got message, delivering it...')
                handler(msg)
            counter += 1
            logger.debug('Worker timer (NOT CLOCK) incremented to %s', counter)
        logger.debug('Worker exiting.')

    @classmethod
    def start_workers(cls, num_workers, message_handler):
        """
        Spawns threads that wait on deliverable messages and call `message_handler` with
        them.

        :param num_workers: number of worker threads to start
        :param message_handler: handler for deliverable messages
        """
        for i in xrange(num_workers):
            logger.debug('Starting worker thread %s', i)
            worker = threading.Thread(
                name='Worker {}'.format(i),
                target=cls._watch_message,
                args=(message_handler,)
            )
            worker.setDaemon(True)
            worker.start()


def multicast_serve(listen_addr, num_workers, msg_handler):
    """
    A convenience method to spin up worker threads and start a server listening for
    multicast messages.

    :param listen_host: the host to listen at
    :param listen_port: the port to listen at
    :param num_workers: the number of worker threads to spawn
    :param msg_handler: a handler for messages once they're found deliverable
    :return: None
    """
    # start listening for multicast messages
    server = util.ReusableThreadingTCPServer(listen_addr, MulticastRequestHandler)
    server_thread = threading.Thread(name='Server', target=server.serve_forever)
    server_thread.start()

    MulticastWorker.start_workers(num_workers, msg_handler)

    return server
