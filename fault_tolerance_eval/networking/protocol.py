import json
import logging

logger = logging.getLogger(__name__)


def node_message(fn):
    def inner(*args, **kwargs):
        val = fn(*args, **kwargs)
        val['client_type'] = 'node'
        return val
    return inner


class Protocol(object):
    # client types
    UNKNOWN_CLIENT = 'unknown client'
    EXTERNAL = 'external'
    NODE = 'node'

    READ_ALL = 'readall'
    READ = 'read'
    READ_RESPONSE = 'read OK'
    READ_ALL_RESPONSE = 'readall OK'
    UPDATE = 'update'
    ADD_INT = 'add int'

    # DataNodeApp entry sequence
    ENTER = 'enter'
    ENTRY_SYNC = 'sync'
    ENTRY_ACCEPTED = 'accepted'
    ENTRY_FINALIZE = 'finalize'
    ENTRY_FINALIZE_ACK = 'ack_finalize'

    @classmethod
    def serialize(cls, message):
        return json.dumps(message) + '\n'

    # Methods for operation on actual data structure.
    @classmethod
    def get_read(cls, index):
        msg = {
            'method': cls.READ,
            'index': index
        }
        return msg

    @classmethod
    def get_read_response(cls, group_id, index, data):
        msg = {
            'method': cls.READ_RESPONSE,
            'group_id': group_id,
            'index': index,
            'data': data,
        }
        return msg

    @classmethod
    def get_read_all_response(cls, group_id, data):
        msg = {
            'method': cls.READ_ALL_RESPONSE,
            'group_id': group_id,
            'data': data,
        }
        return msg


    # Methods for node entry/sync sequence.
    @classmethod
    def get_entry(cls, group_id, proc_id):
        msg = {
            'client_type': cls.NODE,
            'method': cls.ENTER,
            'group_id': group_id,
            'proc_id': proc_id,
        }
        return msg

    @classmethod
    def get_entry_finalization(cls, group_id, proc_id):
        msg = {
            'client_type': cls.NODE,
            'method': cls.ENTRY_FINALIZE,
            'group_id': group_id,
            'proc_id': proc_id,
        }
        return msg

    @classmethod
    def get_entry_accepted(cls, group_id, proc_id):
        msg = {
            'method': cls.ENTRY_ACCEPTED,
            'group_id': group_id,
            'proc_id': proc_id,
        }
        return msg

    @classmethod
    def get_ack_entry_finalization(cls, group_id, proc_id):
        msg = {
            'method': cls.ENTRY_FINALIZE_ACK,
            'group_id': group_id,
            'proc_id': proc_id,
        }
        return msg

    @classmethod
    def get_readall(cls, group_id):
        msg = {
            'method': cls.READ_ALL,
            'group_id': group_id,
        }
        return msg

    @classmethod
    def get_add_int(cls, group_id, value):
        msg = {
            'method': cls.ADD_INT,
            'group_id': group_id,
            'value': value,
        }
        return msg

    @classmethod
    def get_entry_sync(cls, group_id, proc_id, data):
        msg = {
            'method': cls.ENTRY_SYNC,
            'group_id': group_id,
            'proc_id': proc_id,
            'data': data,
        }
        return msg
