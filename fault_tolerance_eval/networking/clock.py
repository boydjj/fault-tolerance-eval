import logging
import threading

logger = logging.getLogger(__name__)


class Clock(object):
    """
    A thread-safe singleton that manages the worker's Lamport clock. See
    https://en.wikipedia.org/wiki/Lamport_timestamps and
    http://python-3-patterns-idioms-test.readthedocs.org/en/latest/Singleton.html

    Thread safety provided by double-checked locking on instantiation. See:
    https://gist.github.com/werediver/4396488 and
    https://en.wikipedia.org/wiki/Double-checked_locking


    All write operations protected with locks.
    """
    # noinspection PyPep8Naming
    class __Clock(object):
        def __init__(self):
            self.counter = 0
            self.lock = threading.Lock()

        def __str__(self):
            return 'Lamport clock: {}'.format(self.counter)

    instance = None
    singleton_lock = threading.Lock()

    def __init__(self):
        if not Clock.instance:
            with Clock.singleton_lock:
                if not Clock.instance:
                    Clock.instance = Clock.__Clock()

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def __str__(self):
        return str(self.instance)

    def tick(self):
        """
        Use when internal events occur.
        """
        with self.instance.lock:
            self.instance.counter += 1
            return self.instance.counter

    def send_msg(self):
        """
        Use when sending a message; include resulting value in outgoing message
        """
        return self.tick()

    def recv_msg(self, other_counter):
        with self.instance.lock:
            self.instance.counter = max(self.instance.counter, other_counter) + 1
            return self.instance.counter

clock = Clock()
