"""
A collection of generic networking utilities.
"""
import logging
import SocketServer

logger = logging.getLogger(__name__)


def recv_until(sock, delimiter='\n'):
    """
    Receives a message until it sees `delimiter` and then returns the message (excluding
    the delimiter). Code derived from "Foundations of Python Network Programming", 2nd
    ed., listing 7-1.
    :param sock: a client socket
    :param delimiter: the delimiter to await, e.g. '\n'
    :return: the received message -- STILL SERIALIZED, excluding the delimiter
    """
    message = ''
    while not message.endswith(delimiter):
        data = sock.recv(4096)
        if not data:
            raise EOFError('socket closed before we saw a {!r}', delimiter)
        message += data
    message = message.rstrip(delimiter)
    logger.debug('Got message %s', message)
    return message


class ReusableThreadingTCPServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True
