import argparse
import json
import logging
import SocketServer
import threading
import time

from ..networking import util
from ..networking import protocol

from .replicated import ReplicatedGroupManager

logger = logging.getLogger(__name__)


class ReplicatedGroupManagerServer(util.ReusableThreadingTCPServer):
    def __init__(self, server_address, handler_class, manager,
                 bind_and_activate=True):
        util.ReusableThreadingTCPServer.__init__(self, server_address,
                                                 handler_class, bind_and_activate)
        self.manager = manager

        # Handler threads must freeze all non-read operations when a node is (re-)entering
        # the group. When a freeze-inducing method is called, we clear() this Event and
        # set() it again once the node's synchronization is complete.
        self.mutating_operations_allowed = threading.Event()
        self.mutating_operations_allowed.set()
        logger.debug('Server initialized to serve %s nodes.', self.manager.num_nodes)


class ReplicatedGroupManagerRequestHandler(SocketServer.StreamRequestHandler):
    """
    Deserialize requests and hand them off to `ReplicatedGroupManager`.
    Serialize responses on the way out. All responses are the responsibility of the
    `ReplicatedGroupManager`.
    """
    def handle(self):
        __handlers = {
            protocol.Protocol.EXTERNAL: self._handle_external_request,
            protocol.Protocol.NODE: self._handle_node_request,
        }

        payload = self.rfile.readline().strip()
        logger.debug('Got payload %s', payload)
        request = json.loads(payload)

        method = __handlers.get(request['client_type'], self._handle_unexpected_request)

        self.write_response(protocol.Protocol.serialize(method(request)))

    def finish(self):
        SocketServer.StreamRequestHandler.finish(self)
        try:
            if self.set_mutating_operations_event:
                logger.debug('Handler has released the mutating operations flag.')
                self.server.mutating_operations_allowed.set()
        except AttributeError:
            pass

    def _handle_external_request(self, request):
        """
        Handle requests coming from external (non-node) clients.
        """
        logger.debug('Got external request %s', request)
        response = None

        if request['method'] == protocol.Protocol.READ_ALL:
            response = self.server.manager.readall(request['group_id'])
        elif request['method'] == protocol.Protocol.ADD_INT:
            response = self.server.manager.add_int(request['group_id'], request['value'])

        return response

    def _handle_node_request(self, request):
        """
        Handle requests coming from node clients. E.g., entry, crash, etc.
        """
        logger.debug('Got node request %s', request)
        response = None

        if request['method'] == protocol.Protocol.ENTER:
            logger.debug('Got entry request from process %s', request['proc_id'])
            # Wait up to 10s for any other blocking operations to complete.
            can_proceed = self.server.mutating_operations_allowed.wait(10)
            if not can_proceed:
                logger.warn('Previous node management op failed. Proceeding...')
            self.server.mutating_operations_allowed.clear()
            response = self.server.manager.handle_entry(request['group_id'],
                                                        request['proc_id'])
        elif request['method'] == protocol.Protocol.ENTRY_FINALIZE:
            response = self.server.manager.handle_entry_finalization(request['group_id'],
                                                                     request['proc_id'])
            self.set_mutating_operations_event = True
        return response

    def _handle_unexpected_request(self, request):
        logger.debug('Got unexpected request %s', request)
        response = {'method': protocol.Protocol.UNKNOWN_CLIENT}
        return response

    def write_response(self, content):
        self.wfile.write('{response}'.format(response=content))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s:%(levelname)-5s(%(threadName)-10s:%(name)s): %(message)s')
    arg_parser = argparse.ArgumentParser(description='ReplicatedGroupManagerApp')
    arg_parser.add_argument('host', action='store')
    arg_parser.add_argument('port', action='store', type=int)
    arg_parser.add_argument('num_nodes', action='store', type=int)
    arg_parser.add_argument('node_config', action='store')
    args = arg_parser.parse_args()

    group_manager = ReplicatedGroupManager(args.num_nodes, args.node_config)

    server = ReplicatedGroupManagerServer((args.host, args.port),
                                          ReplicatedGroupManagerRequestHandler,
                                          group_manager)

    server_thread = threading.Thread(target=server.serve_forever)
    logger.debug('Starting server...')
    server_thread.start()

    try:
        logger.info('Beginning wait loop. Hit Ctrl-C to kill server.')
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        logger.debug('Got KeyboardInterrupt, shutting down...')
        server.shutdown()
        logger.debug('Closing server...')
        server.server_close()
    finally:
        logger.debug('Waiting for server thread to finish.')
        server_thread.join()
    logger.debug('Shutdown complete.')
