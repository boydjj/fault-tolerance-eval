import contextlib
import json
import logging
import socket

from ..networking import protocol
from ..networking import util

logger = logging.getLogger(__name__)


class NodeProxy(object):
    """
    Handle network communication to a node. Methods should return deserialized objects.
    """
    def __init__(self, multicast_addr, read_addr, group_id, proc_id, active=False):
        self.multicast_addr = multicast_addr
        self.read_addr = read_addr
        self.group_id = group_id
        self.proc_id = proc_id
        self.active = active

    def __get_socket(self):
        return socket.create_connection(self.read_addr)

    def read(self, index):
        """
        Perform network communication to get value at an index.
        """
        msg = protocol.Protocol.get_read(index)
        logger.debug('Request: %s', msg)
        with contextlib.closing(self.__get_socket()) as sock:
            logger.debug('Sending request to node...')
            sock.sendall(msg)
            logger.debug('Waiting for response...')
            response = util.recv_until(sock)

        logger.debug('Response: %s', response)
        return json.loads(response)

    def readall(self):
        """
        Perform network communication to get entire data structure.
        """
        msg = protocol.Protocol.get_readall(self.group_id)
        logger.debug('Request: %s', msg)
        with contextlib.closing(self.__get_socket()) as sock:
            logger.debug('Sending readall request to node...')
            msg = protocol.Protocol.serialize(msg)
            sock.sendall(msg)
            logger.debug('Waiting for response...')
            response = util.recv_until(sock)

        logger.debug('Response: %s', response)
        return json.loads(response)

    def __unicode__(self):
        return u'Node {proc_id} (group {group_id}) at {addr}'.format(
            proc_id=self.proc_id,
            group_id=self.group_id,
            addr=self.read_addr
        )

    def __str__(self):
        return unicode(self)

    def __repr__(self):
        return u'{class_name}({multicast_addr}, {read_addr}, {group_id}, {proc_id}, ' \
               u'{active})'.format(
            class_name=self.__class__.__name__,
            multicast_addr=repr(self.multicast_addr),
            read_addr=repr(self.read_addr),
            group_id=repr(self.group_id),
            proc_id=repr(self.proc_id),
            active=repr(self.active),
        )
