import collections
import ConfigParser
import datetime
import logging

from profiling.loggers import update_time_logger, recovery_time_logger
from ..networking import multicast
from ..networking import protocol

from . import proxy

logger = logging.getLogger(__name__)


class ReplicatedGroupManager(object):
    def __init__(self, num_nodes, node_config):
        self.num_nodes = num_nodes

        config_parser = ConfigParser.SafeConfigParser()
        config_parser.read(node_config)
        self.nodes = collections.defaultdict(dict)

        self.active_nodes = set()
        self.inactive_nodes = set()
        self.transitioning_nodes = set()

        node_sections = ['node {}'.format(i) for i in xrange(1, self.num_nodes+1)]
        for section in node_sections:
            multicast_host = config_parser.get(section, 'multicast_host')
            multicast_port = config_parser.getint(section, 'multicast_port')
            read_host = config_parser.get(section, 'read_host')
            read_port = config_parser.getint(section, 'read_port')

            node = proxy.NodeProxy(
                (multicast_host, multicast_port),
                (read_host, read_port),
                config_parser.get(section, 'group_id'),
                config_parser.getint(section, 'proc_id'),
            )
            self.nodes[node.group_id][node.proc_id] = node

            # All nodes are inactive to start.
            self.inactive_nodes.add(node)

        self.multicast_clients = {}
        for group_id, group in self.nodes.iteritems():
            _multicast_addrs = [node.multicast_addr for node in group.values()]
            self.multicast_clients[group_id] = multicast.MulticastClient(_multicast_addrs)

        logger.debug('Nodes initialized to: %s', self.nodes)

    def __transition_node(self, node):
        self.multicast_clients[node.group_id].remove_host(node.multicast_addr)
        if node in self.active_nodes:
            self.active_nodes.remove(node)
        elif node in self.inactive_nodes:
            self.inactive_nodes.remove(node)

        node.active = False
        self.transitioning_nodes.add(node)


    def __activate_node(self, node):
        self.multicast_clients[node.group_id].add_host(node.multicast_addr)
        if node in self.transitioning_nodes:
            self.transitioning_nodes.remove(node)
        elif node in self.inactive_nodes:
            self.inactive_nodes.remove(node)

        node.active = True
        self.active_nodes.add(node)

    def __deactivate_node(self, node):
        self.multicast_clients[node.group_id].remove_host(node.multicast_addr)
        if node in self.transitioning_nodes:
            self.transitioning_nodes.remove(node)
        elif node in self.active_nodes:
            self.active_nodes.remove(node)

        node.active = False
        self.inactive_nodes.add(node)

    def read(self, group_id, index):
        """
        Perform a consistent (although not nec. up-to-date) read operation for an index of
        the data structure.
        """
        response = protocol.Protocol.get_read_response(group_id, index, [])
        group_nodes = self.nodes[group_id]
        if not group_nodes:
            logger.debug('No nodes found for group %s. Possible groups are: %s',
                         group_id, self.nodes.keys())
            return protocol.Protocol.get_read_response(group_id, index, [])

        logger.debug('Nodes: %s', group_nodes)
        for node_id, node_proxy in group_nodes.iteritems():
            if node_proxy not in self.active_nodes:
                continue

            # noinspection PyBroadException
            try:
                logger.debug('Attempting read from %s', node_proxy)
                node_data = node_proxy.read(index)
                response = protocol.Protocol.get_read_response(group_id, index,
                                                               node_data)
            except:
                logger.exception('Could not read from node %s', node_id)
                continue
            else:
                break

        return response

    def readall(self, group_id):
        """
        Perform a consistent (although not nec. up-to-date) read-all operation for a group.
        """
        response = protocol.Protocol.get_read_all_response(group_id, [])
        group_nodes = self.nodes[group_id]
        if not group_nodes:
            logger.debug('No nodes found for group %s. Possible groups are: %s',
                         group_id, self.nodes.keys())
            return protocol.Protocol.get_read_all_response(group_id, [])

        logger.debug('Nodes: %s', group_nodes)
        for node_id, node_proxy in group_nodes.iteritems():
            if node_proxy not in self.active_nodes:
                continue

            # noinspection PyBroadException
            try:
                logger.debug('Attempting readall from %s', node_proxy)
                node_data = node_proxy.readall()
                response = protocol.Protocol.get_read_all_response(group_id, node_data)
            except:
                logger.exception('Could not readall from node %s', node_id)
                continue
            else:
                break

        return response

    def add_int(self, group_id, value):
        message = protocol.Protocol.get_add_int(group_id, value)
        group_nodes = self.nodes[group_id]
        if not group_nodes:
            logger.debug('No nodes found for group %s. Possible groups are: %s',
                         group_id, self.nodes.keys())
            return protocol.Protocol.get_read_all_response(group_id, [])

        logger.debug('Nodes: %s', group_nodes)

        multicast_client = self.multicast_clients[group_id]

        # Capture before/after time so that we can log the delta
        t0 = datetime.datetime.now()
        multicast_client.send(protocol.Protocol.serialize(message))
        t1 = datetime.datetime.now()
        delta = t1 - t0
        update_time_logger.info('%s,%s', len(group_nodes), delta.total_seconds())

        return True

    def handle_entry(self, group_id, proc_id):
        response = None
        logger.debug('Nodes: %s', self.nodes)
        t0 = datetime.datetime.now()
        node = self.nodes[group_id][proc_id]
        self.__transition_node(node)
        active_group_nodes = set(self.nodes[group_id].values()) - \
                             self.transitioning_nodes - \
                             self.inactive_nodes
        logger.info('Active group nodes: %s', active_group_nodes)
        if not active_group_nodes:
            response = protocol.Protocol.get_entry_accepted(group_id, proc_id)

        for node in active_group_nodes:
            try:
                logger.debug('Attempting to readall from %s', node)
                node_response = node.readall()
                response = protocol.Protocol.get_entry_sync(group_id, proc_id,
                                                            node_response['data'])
            except:
                logger.exception('Could not read from node %s', node)
                continue
            else:
                logger.debug('Got more data for new node. Sending back %s',
                             response['data'])
                if node_response['data']:
                    t1 = datetime.datetime.now()
                    delta = t1 - t0
                    recovery_time_logger.info('%s,%s,%s',
                                              len(self.nodes[group_id]),
                                              len(node_response['data']),
                                              delta.total_seconds())
                break
        return response

    def handle_entry_finalization(self, group_id, proc_id):
        node = self.nodes[group_id][proc_id]
        self.__activate_node(node)
        return protocol.Protocol.get_ack_entry_finalization(group_id, proc_id)
