About these logs
================
The files in this directory's subdirectories are log files resulting from runs of 
`profiling/fusion_comparison_fixed_n.py` or `profiling/fusion_comparison_fixed_k.py`.

Each top-level subdirectory indicates the experimental run the data is for, and each 
subdirectory is named for the arguments passed to the script. So, for example, 
the directory `fixed_n/n = 5, ops = 10000, s = 1000` contains logs from a run invoked with
the following:

    python -m profiling.fusion_comparison_fixed_n -n 5 -o 10000 -s 1000

The directory `fixed_f/g = 1, ops = 10000, s = 1000` contains logs from a run invoked with
the following:

    python -m profiling.fusion_comparison_fixed_f -g 1 -o 10000 -s 1000


Run timings
===========

Fixed-n
-------
Four runs of the comparison script were run during the fixed-n experimental run. Their 
timings are below (in seconds):

* n = 5, ops = 10000, s = 1000: **964.769573**
* n = 10, ops = 10000, s = 1000: **1982.985803**
* n = 15, ops = 10000, s = 1000: **3273.852958**
* n = 20, ops = 10000, s = 1000: **5328.280214**

Fixed-f
-------
Five runs of the comparison script were run during the fixed-f experimental run. Their 
timings are below (in seconds):

* g = 1, ops = 10000, s = 1000: **532.337852**
* g = 2, ops = 10000, s = 1000: **1062.886125**
* g = 3, ops = 10000, s = 1000: **1602.622892**
* g = 4, ops = 10000, s = 1000: **2117.918594**
* g = 5, ops = 10000, s = 1000: **2634.707975**
