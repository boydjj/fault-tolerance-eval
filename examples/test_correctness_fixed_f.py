"""
Make sure active replication app works for a given number of groups. Example usage:

python examples/test_correctness_fixed_f.py 2
"""

import json
import logging
import random
import socket
import sys

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

MGR_ADDR = ('localhost', 9999)

GROUPS = {
    1: 'A',
    2: 'B',
    3: 'C',
    4: 'D',
    5: 'E',
}


def test_correctness(num_messages, num_groups):
    """
    Send num_messages messages to num_groups groups, as defined by `GROUPS` above.
    """

    # For every group... [note: xrange(start, end) is exclusive of end]
    for i in xrange(1, num_groups + 1):
        group_id = GROUPS[i]
        logger.debug('Sending messages to group %s', group_id)
        # ... send num_messages messages.
        for j in xrange(num_messages):
            msg = {'client_type': 'external', 'group_id': group_id, 'method': 'add int',
                   'value': random.randint(0, 1)}
            serialized_msg = json.dumps(msg)

            sock = socket.create_connection(MGR_ADDR)
            sock.sendall(serialized_msg + '\n')
            resp = sock.recv(2048)
            logger.info(resp)

if __name__ == '__main__':
    num_messages = int(sys.argv[1])
    num_groups = int(sys.argv[2])
    logger.info('Got called with %s, %s', num_messages, num_groups)
    test_correctness(num_messages, num_groups)
