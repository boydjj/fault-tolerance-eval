import json
import logging
import random
import socket
import sys

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MGR_ADDR = ('localhost', 9999)


def test_correctness(x):
    for i in xrange(x):
        msg = {'client_type': 'external', 'group_id': 'A', 'method': 'add int', 'value': random.randint(0, 1)}
        serialized_msg = json.dumps(msg)

        sock = socket.create_connection(MGR_ADDR)
        sock.sendall(serialized_msg + '\n')
        resp = sock.recv(2048)
        logger.info(resp)

if __name__ == '__main__':
    x = int(sys.argv[1])
    test_correctness(x)
